package com.company;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

public class VapeLiquidStorage {

    public static void main(String[] args) {
        VapeLiquid vapeLiquid3 = new VapeLiquid(30, 20, 40, 10, "Maxwells");
        VapeLiquid vapeLiquid1 = new VapeLiquid(20, 20, 30, 4, "DOpe kitchen");
        VapeLiquid vapeLiquid2 = new VapeLiquid(10, 20, 30, 6, "super dope");
        ArrayList<VapeLiquid> vapeLiquids = new ArrayList<>();
        vapeLiquids.add(vapeLiquid1);
        vapeLiquids.add(vapeLiquid2);
        vapeLiquids.add(vapeLiquid3);
        Collections.sort(vapeLiquids, new VapeLiquidComparatorByVolume());
        System.out.println(vapeLiquids);
        Collections.sort(vapeLiquids, new VapeLiquidComparatorByWholePrice());
        System.out.println(vapeLiquids);
        Collections.sort(vapeLiquids, new VapeLiquidComparatorByName());
        System.out.println(vapeLiquids);
    }

    static class VapeLiquidComparatorByVolume implements Comparator<VapeLiquid> {
        @Override
        public int compare(VapeLiquid o1, VapeLiquid o2) {
            Integer volume1 = o1.getVolume();
            Integer volume2 = o2.getVolume();
            return volume1.compareTo(volume2);
        }
    }

    static class VapeLiquidComparatorByWholePrice implements Comparator<VapeLiquid> {
        @Override
        public int compare(VapeLiquid o1, VapeLiquid o2) {
            int wholePrice1 = o1.getPrice() * o1.getQuantity();
            int wholePrice2 = o2.getPrice() * o2.getQuantity();
            if (wholePrice1 > wholePrice2) {
                return 1;
            } else if (wholePrice1 < wholePrice2) {
                return -1;
            }
            return 0;
        }
    }
        static class VapeLiquidComparatorByName implements Comparator<VapeLiquid> {
            @Override
            public int compare(VapeLiquid o1, VapeLiquid o2) {
                return o1.getName().compareTo(o2.getName());
            }
        }
}
