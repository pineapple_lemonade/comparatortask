package com.company;

public class VapeLiquid {
	private Integer volume;
	private Integer volumeOfNicotine;
	private Integer price;
	private Integer quantity;
	private String name;


	public VapeLiquid(int volume, int volumeOfNicotine, int price, int quantity, String name) {
		this.volume = volume;
		this.volumeOfNicotine = volumeOfNicotine;
		this.price = price;
		this.quantity = quantity;
		this.name = name;
	}
	public String getName() {
		return name;
	}
	public int getVolume() {
		return volume;
	}

	public void setVolume(int volume) {
		this.volume = volume;
	}

	public int getVolumeOfNicotine() {
		return volumeOfNicotine;
	}

	public void setVolumeOfNicotine(int volumeOfNicotine) {
		this.volumeOfNicotine = volumeOfNicotine;
	}

	public int getPrice() {
		return price;
	}

	public void setPrice(int price) {
		this.price = price;
	}

	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	@Override
	public String toString() {
		return "VapeLiquid{" +
				"volume=" + volume +
				", volumeOfNicotine=" + volumeOfNicotine +
				", price=" + price +
				", quantity=" + quantity +
				", name='" + name + '\'' +
				'}';
	}
}
